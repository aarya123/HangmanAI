package Dictionary;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * User: AnubhawArya
 * Date: 10/19/13
 * Time: 1:14 PM
 */
public class WordList {

    private ArrayList<String> wordList;

    public WordList(ArrayList<String> wordList) {
        this.wordList = wordList;
    }

    AbstractMap.SimpleEntry<Character, Double> letterToGuess(String correct, String incorrect) {
        HashMap<Character, Integer> charCount = getCharFrequency();
        int total = 0;
        for (Map.Entry<Character, Integer> temp : charCount.entrySet()) {
            total += temp.getValue();
        }
        char maxChar = 0;
        double maxPercent = 0.0;
        for (Map.Entry<Character, Integer> temp : charCount.entrySet()) {
            char tempChar = temp.getKey();
            int tempInt = temp.getValue();
            if (((tempChar >= 65 && tempChar <= 90) || (tempChar >= 97 && tempChar <= 122)) && ((double) tempInt) / total > maxPercent
                    && !correct.contains(tempChar + "") && !incorrect.contains(tempChar + "")) {
                maxPercent = ((double) tempInt) / total;
                maxChar = tempChar;
            }
        }
        return new AbstractMap.SimpleEntry<>(maxChar, maxPercent);
    }

    private HashMap<Character, Integer> getCharFrequency() {
        HashMap<Character, Integer> charCount = new HashMap<>();
        for (String tempString : wordList) {
            char[] charArray = tempString.toCharArray();
            for (char tempChar : charArray) {
                charCount.merge(tempChar, 1, Integer::sum);
            }
        }
        return charCount;
    }

    public void refine(String word, String correct, String incorrect) {
        char[] wordArray = word.toLowerCase().toCharArray(), correctArray = correct.toLowerCase().toCharArray(),
                incorrectArray = incorrect.toLowerCase().toCharArray();
        // Remove words with incorrect letters
        for (char value : incorrectArray) {
            for (int j = 0; j < wordList.size(); j++) {
                if (wordList.get(j).contains(value + "")) {
                    wordList.remove(j);
                    j--;
                }
            }
        }
        for (int i = 0; i < wordArray.length; i++) {
            for (int j = 0; j < wordList.size(); j++) {
                String tempWord = wordList.get(j);
                if (wordArray[i] == '_') { // Remove words that have letters we already guessed in incorrect places
                    for (char c : correctArray) {
                        if (c == tempWord.toCharArray()[i]) {
                            wordList.remove(tempWord);
                            j--;
                        }
                    }
                } else if (wordArray[i] != tempWord.toCharArray()[i]) { // Remove words where the word isn't equal to the guess
                    wordList.remove(tempWord);
                    j--;
                }
            }
        }
    }
}
